## Sandstone Benchmarks

This benchmark is derived from X-ray tomographic computed images (XRCT) of natural sandstone samples. The samples include Bandera Gray, Parker, Kirby, Bandera Brown, Berea, Berea Sister Gray (BSG), Berea Upper Gray (BUG), Castlegate, Buff Berea (BB), Leopard and Bentheimer sandstones.

We use binary images of the connected pore space, treating the sandstone itself as an obstacle, and the permeable pore space as traversable terrain. Traversable voxels compose a single connected region, assuming a 6-connected neighbourhood. 

We define 11 maps with 2,000 pathfinding instances each, for a total of 22,000 problems. The pore-space geometry is characterised by winding narrow corridors and natural nonsymmetric shapes resulting in an environment reminiscent of random grid maps.

For more information, and if you are using these benchmark problems, please cite the following paper to reference the benchmark sets:

@article{nobes2023voxels,
   title={Voxel Benchmarks for 3D Pathfinding: Sandstone, Descent, and Industrial Plants},
   author={Thomas K. Nobes and Daniel D. Harabor and Michael Wybrow and Stuart D.C. Walsh},
   journal={The 16th International Symposium on Combinatorial Search (SoCS)},
   volume={16},
   number={1},
   pages={},
   year={2023},
   url = {https://ojs.aaai.org/index.php/SOCS/article/view/27283 }
}


[Download all maps (335M)](https://bitbucket.org/shortestpathlab/benchmarks/downloads/sandstone_maps.zip)
[Download all scenarios (384K)](https://bitbucket.org/shortestpathlab/benchmarks/downloads/sandstone_scens.zip)

| Preview (Static) | Preview (GIF) | Map Name | Mem | Dimensions | # Voxels | Scenario |
| :--- | :---------- | :---------- | :---------- | :---------- | :---------- | :---------- |
| ![BB_still](imgs/BB.png) | ![BB_gif](gifs/BB.gif) | [BB.3dmap](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/sandstone/map_files/BB.3dmap.zip) | 34.2M | 400 × 400 × 400 | 64,000,000 | [BB.3dscen](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/sandstone/scen_files/BB.3dscen) |
| ![BSG_still](imgs/BSG.png) | ![BSG_gif](gifs/BSG.gif) | [BSG.3dmap](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/sandstone/map_files/BSG.3dmap.zip) | 28.4M | 400 × 400 × 400 | 64,000,000 | [BSG.3dscen](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/sandstone/scen_files/BSG.3dscen) |
| ![BUG_still](imgs/BUG.png) | ![BUG_gif](gifs/BUG.gif) | [BUG.3dmap](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/sandstone/map_files/BUG.3dmap.zip) | 29.2M | 400 × 400 × 400 | 64,000,000 | [BUG.3dscen](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/sandstone/scen_files/BUG.3dscen) |
| ![BanderaBrown_still](imgs/BanderaBrown.png) | ![BanderaBrown_gif](gifs/BanderaBrown.gif) | [BanderaBrown.3dmap](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/sandstone/map_files/BanderaBrown.3dmap.zip) | 29.6M | 400 × 400 × 400 | 64,000,000 | [BanderaBrown.3dscen](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/sandstone/scen_files/BanderaBrown.3dscen) |
| ![BanderaGray_still](imgs/BanderaGray.png) | ![BanderaGray_gif](gifs/BanderaGray.gif) | [BanderaGray.3dmap](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/sandstone/map_files/BanderaGray.3dmap.zip) | 33.1M | 400 × 400 × 400 | 64,000,000 | [BanderaGray.3dscen](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/sandstone/scen_files/BanderaGray.3dscen) |
| ![Bentheimer_still](imgs/Bentheimer.png) | ![Bentheimer_gif](gifs/Bentheimer.gif) | [Bentheimer.3dmap](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/sandstone/map_files/Bentheimer.3dmap.zip) | 41.5M | 400 × 400 × 400 | 64,000,000 | [Bentheimer.3dscen](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/sandstone/scen_files/Bentheimer.3dscen) |
| ![Berea_still](imgs/Berea.png) | ![Berea_gif](gifs/Berea.gif) | [Berea.3dmap](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/sandstone/map_files/Berea.3dmap.zip) | 32.4M | 400 × 400 × 400 | 64,000,000 | [Berea.3dscen](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/sandstone/scen_files/Berea.3dscen) |
| ![CastleGate_still](imgs/CastleGate.png) | ![CastleGate_gif](gifs/CastleGate.gif) | [CastleGate.3dmap](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/sandstone/map_files/CastleGate.3dmap.zip) | 38.3M | 400 × 400 × 400 | 64,000,000 | [CastleGate.3dscen](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/sandstone/scen_files/CastleGate.3dscen) |
| ![Kirby_still](imgs/Kirby.png) | ![Kirby_gif](gifs/Kirby.gif) | [Kirby.3dmap](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/sandstone/map_files/Kirby.3dmap.zip) | 33.2M | 400 × 400 × 400 | 64,000,000 | [Kirby.3dscen](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/sandstone/scen_files/Kirby.3dscen) |
| ![Leopard_still](imgs/Leopard.png) | ![Leopard_gif](gifs/Leopard.gif) | [Leopard.3dmap](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/sandstone/map_files/Leopard.3dmap.zip) | 30.8M | 400 × 400 × 400 | 64,000,000 | [Leopard.3dscen](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/sandstone/scen_files/Leopard.3dscen) |
| ![Parker_still](imgs/Parker.png) | ![Parker_gif](gifs/Parker.gif) | [Parker.3dmap](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/sandstone/map_files/Parker.3dmap.zip) | 20.4M | 400 × 400 × 400 | 64,000,000 | [Parker.3dscen](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/sandstone/scen_files/Parker.3dscen) |
