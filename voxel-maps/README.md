In this directory: 

| <div style="width:150px">Path</div> | Description |
| :--- | :---------- |
| ├ [descent](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/descent/) | Home of the *Descent* benchmarks. Read the [paper](). |
| ├ [industrial-plants](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/industrial-plants/) | Home of the *Industrial Plant* benchmarks. Read the [paper](). |
| ├ [sandstone](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/sandstone/) | Home of the *Sandstone* benchmarks. Read the [paper](). |
| └ [warframe](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/warframe/) | (Mirror) The Warframe benchmarks from Nathan Sturtevant's [MovingAI](https://movingai.com/benchmarks/voxels.html) repository. |



## Voxel map file format (.3dmap)

This repository contains several publicly-available 3D voxel world benchmarks. Voxels are unit cube volume elements, the 3D equivalent to a 2D grid map. Each voxel cell has uniform cost. 

Each map file (.3dmap) details a set of voxels that can or cannot be traversed. The format is provided below (with comments).

    [map-type] [map-width] [map-height] [map-depth]
    [x_1] [y_1] [z_1] # voxel 1
    [x_2] [y_2] [z_2] # voxel 2
    .
    .
    .
    [x_n] [y_n] [z_n] # voxel n

The first line specifies the map type ``[map-type]``: this can be either ``voxel`` or ``rev_voxel``, which specifies whether we list voxels that correspond to filled or free voxels respectively. This is useful for representing maps minimally based on the proportion of free space (i.e., sometimes there are less free voxels than filled).

The first line also specifies the width (x-axis), height (y-axis) and depth (z-axis) of the map (``map-width``, ``map-height``, ``map-depth``). Coordinate indexing starts at 0. The total number of voxels in a map is width × height × depth.

The following ``n`` number of lines detail the X, Y, Z coordinates of all voxels that correspond to the traversiblity criteria (free or filled) as specified by the ``[map-type]``. Note that the number of voxels that are free/filled (``n``) is not the same as the total number of voxels in the map.

We provide an example of a voxel map file below.

    voxel 767 256 668
    167 1 56
    168 1 56
    169 1 56

From this, we gather that the voxels listed correspond to obstacles (filled). There are only three obstacle voxels (``n``=3), and there are a total of 131,163,136 voxels in the map.

## Voxel scenario file format (.3dscen)

Each scenario file (.3scen) details a set of curated start-target pairs for 3D pathfinding experiments. The format is provided below (with comments).

    version [version-num]
    [map-name]
    [sx_1] [sy_1] [sz_1] [tx_1] [ty_1] [tz_1] [cost_1] [h-error_1] [d-bias_1 (conditional)] # instance 1
    [sx_2] [sy_2] [sz_2] [tx_2] [ty_2] [tz_2] [cost_2] [h-error_2] [d-bias_2 (conditional)] # instance 2
    .
    .
    .
    [sx_n] [sy_n] [sz_n] [tx_n] [ty_n] [tz_n] [cost_n] [h-error_n] [d-bias_n (conditional)] # instance n

The first line specifies the version ``version-num`` of the scenario file. Version information is benchmark specific (as is described in their respecitve locations). This allows for better control in the literature for comparative purposes, ensuring evaluation on the same versions of the problems. Note that the continued file description below may vary with version, but we detail generalised formatting.

The second line simply details the name of the corresponding map file ``map-name`` (which must adhere to the above specifications for .3dmaps).

The remaining ``n`` number of lines detail individual problem instances. The first six elements list the X, Y, Z coordinates of the start (``sx``, ``sy``, ``sz``) and target (``tx``, ``ty``, ``tz``) nodes.

Next, the optimal path cost ``cost`` of the problem instance is given.

This is followed by the heuristic error ``h-error`` which provides a general measure of problem difficulty based on the deviation from heuristic estiamted distance (oftentimes, the voxel herustic is used which is a generalisation of the 2D octile distance to include 3D diagonals).

Finally, some versions also include a the ratio of directional bias ``d-bias`` in a problem. This is calculated as the ratio of the higher number of nodes expanded to the lower expansion cost when searching from start-to-target or vice-versa. This provides an indication of how much easier it may be to search a problem in one direction over another.

We provide an example of a voxel scenario file below.

    version 2
    level01.3dmap
    348 178 572 348 177 573 1.41421 1.0 1.0
    302 215 126 303 216 126 1.41421 1.0 1.0
    134 77 271 136 77 269 2.82843 1.0 1.0

From this, we gather that there are only three problem instances (``n``=3), which correspond to the map level01.3dmap. The first instance details a path from start voxel [348, 178, 348] to target voxel [348, 177, 573]. The optimal cost is 1.41421, the heuristic error is 1 (i.e., no deviations), and the ratio of directional bias is 1 (i.e., same expansion cost in both directions).
