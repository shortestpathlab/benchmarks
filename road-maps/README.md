A common benchmark for testing performance on road networks comes from the 9th DIMACS Challenge.
These files are too big to mirror here, but they are available for download from the [competition 
homepage](http://www.diag.uniroma1.it/~challenge9/).
