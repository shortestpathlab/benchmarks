You have reached the **benchmarks** repository of the [Shortest Path Lab](https://pathfinding.ai).
Here we make available community-generated benchmarks for evaluating pathfinding algorithms.

The benchmarks comprise **map** files and **scenario** files. A map file is
a description of the operating environment of the pathfinding agent. These can
can be in one of several standard formats such as grid, graph, voxel, navigation mesh
or polygon obstacle descriptions. A [scenario]() file is collection of location
pairs which must be solved by finding a shortest path, from start to target
on the associated map.

In this repository: 

| <div style="width:150px"><div style="width:150px">Path</div></div> | Description |
| :--- | :---------- |
| ├ [grid-maps](https://bitbucket.org/shortestpathlab/benchmarks/src/master/grid-maps/) | Collections of square grids with uniform costs.  |
| ├ [mesh-maps](https://bitbucket.org/shortestpathlab/benchmarks/src/master/mesh-maps/) | Collections of navigation meshes with uniform costs. |
| ├ [poly-maps](https://bitbucket.org/shortestpathlab/benchmarks/src/master/poly-maps/) | Collections of polygonal obstacles in the plane. |
| ├ [road-maps](https://bitbucket.org/shortestpathlab/benchmarks/src/master/road-maps/) | Collections of road network graphs. |
| └ [voxel-maps](https://bitbucket.org/shortestpathlab/benchmarks/src/master/voxel-maps/) | Collections of 3D voxel (cube) grids with uniform costs. |
