#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <utility>
#include <limits>
#include <fstream>
#include <cstddef>
#include <cmath>
#include <cstring>
#include <cstdlib>
#include <cassert>

using std::size_t;

//
//	user defined cost, defaults here as double, change to allow more complex values (such as pairs) to work on
//
struct Cost
{
	double value;

	Cost() = default;
	Cost(double v) : value(v)
	{ }

	bool operator<(const Cost& rhs) const
	{
		return value < rhs.value;
	}
	bool operator==(const Cost& rhs) const
	{
		return value == rhs.value;
	}

	Cost operator-(const Cost& rhs) const
	{
		return Cost(value - rhs.value);
	}

	static Cost infinity()
	{
		return Cost(std::numeric_limits<double>::infinity());
	}

	// the value used for splitting into buckets
	// should be partial order
	// can be set to 0, which will result in even splits (instead of bucketing similar values, will just split into groups evenly)
	double splitValue() const
	{
		return value;
	}
};
// how to read in parameters
std::istream& operator>>(std::istream& in, Cost& cost)
{
	in >> cost.value;
	return in;
}

struct CostId
{
	size_t id;
	Cost cost;
};

bool operator<(const CostId& lhs, const CostId& rhs) noexcept
{
	return lhs.cost < rhs.cost ? true : lhs.cost == rhs.cost ? lhs.id < rhs.id : false;
}

struct Bucket
{
	std::vector<CostId*> items;

	bool empty() const noexcept { return items.empty(); }
	size_t size() const noexcept { return items.size(); }
	Cost& min() const noexcept { return items.front()->cost; }
	Cost& max() const noexcept { return items.back()->cost; }
};

struct Cluster
{
	std::vector<size_t> buckets; // bucket ids
};

class Selector
{
public:
	using bucket_it = std::list<Bucket>::iterator;

	size_t size() const noexcept { return costList.size(); }

	void loadCosts(std::istream& in)
	{
		Cost cost;
		while (in >> cost) {
			costList.push_back(cost);
		}
		if (in.bad() || (!in.eof()))
			throw std::runtime_error("error reading in cost");
		in.clear(std::ios::eofbit);
		orderList.resize(costList.size());
		for (size_t i = 0; i < costList.size(); ++i) {
			orderList[i] = CostId{i, costList[i]};
		}
		std::stable_sort(orderList.begin(), orderList.end());
	}

	void generateBuckets(size_t l_bucketSize)
	{
		bucketSize = l_bucketSize;
		// set up inital bucket
		bucketList.emplace_front();
		{
			auto& B = bucketList.front();
			B.items.reserve(orderList.size());
			for (CostId& C : orderList) {
				B.items.push_back(&C);
			}
		}
		// recursively split buckets
		while (true)
		{
			bool bucketsSplit = false;
			for (auto it = bucketList.begin(), ite = bucketList.end(); it != ite; ) {
				auto r = splitBucket(it);
				it = r.first;
				if (r.second)
					bucketsSplit = true;
			}
			// at least 1 bucket was split, split next level
			if (!bucketsSplit)
				break;
		}
		// merge small buckets
		for (auto it = bucketList.begin(), ite = bucketList.end(); it != ite; ) {
			if (std::next(it) != ite && it->size() < bucketSize) {
				mergeBucket(it);
			} else {
				++it;
			}
		}
		// check final bucket and merge if small
		while (bucketList.back().size() < bucketSize) {
			mergeBucket(std::prev(bucketList.end(), 2));
		}
		// split all oversized buckets evenly
		for (auto it = bucketList.begin(), ite = bucketList.end(); it != ite; ) {
			it = splitBucket(it, true).first;
		}
		// internal check for invalid bucket size
		for (Bucket& B : bucketList) {
			if (B.size() < bucketSize || B.size() >= (2*bucketSize)) {
				assert(false);
				throw std::runtime_error("invalid bucket operation");
			}
		}
		// bucket list done
		bucketL.reserve(bucketList.size());
		for (auto it = bucketList.begin(), ite = bucketList.end(); it != ite; ++it) {
			bucketL.push_back(&*it);
		}
	}

	// splits bucket_it, returns bucket after split and if it was split
	std::pair<bucket_it, bool> splitBucket(bucket_it bucket, bool splitEven = false)
	{
		Bucket& B = *bucket;
		size_t bs = B.size();
		size_t bcount = bs / bucketSize;
		// not enough elements to split
		if (bcount <= 1) {
			return {std::next(bucket), false};
		}
		double span = B.max().splitValue() - B.min().splitValue();
		// if span is 0, then we can not bucket, must split
		splitEven |= span < 1e-8;
		bucket_it bpos = bucket;
		size_t totalSize = 0;
		if (!splitEven) {
			double inc = span / bcount;
			auto it = B.items.begin(), ite = B.items.end();
			for (size_t i = 0; it != ite && i < bcount; ++i) {
				double costTo = B.min().splitValue() + (i+1) * inc;
				tmpBucket.clear();
				for ( ; it != ite && (i+1 == bcount || it[0]->cost < costTo); ++it) {
					tmpBucket.push_back(*it);
				}
				if (tmpBucket.size() == bs) { // all elements in same bucket, split evenly, this should never occur, here just in case
					splitEven = true;
				} else if (!tmpBucket.empty()) {
					// non-empty bucket, insert
					bpos = bucketList.emplace(std::next(bpos));
					bpos->items = tmpBucket;
					totalSize += bpos->size();
				}
			}
		}
		if (splitEven) {
			// splits the buckets evenly
			size_t splitSize = bucketSize + (bs % bucketSize); // first bucket holds the extra elements
			totalSize = 0;
			auto it = B.items.begin();
			for (size_t i = 0; i < bcount; ++i) {
				bpos = bucketList.emplace(std::next(bpos));
				bpos->items.assign(it, it + splitSize);
				it += splitSize;
				totalSize += splitSize;
				splitSize = bucketSize;
			}
		}
		if (totalSize != B.size())
			throw std::runtime_error("invalid splitting of buckets");
		bucketList.erase(bucket);
		return {std::next(bpos), true};
	}

	// merge bucket with next(bucket) and erase next(bucket)
	void mergeBucket(bucket_it bucket)
	{
		auto& B = *bucket;
		auto& B2 = *std::next(bucket);
		B.items.insert(B.items.end(), B2.items.begin(), B2.items.end());
		bucketList.erase(std::next(bucket));
	}

	// generate the scenarios
	void generateClusters(size_t l_clusterNum)
	{
		clusterNum = l_clusterNum;
		// setup k-clustering with DP
		size_t k = clusterNum - 2;
		size_t n = bucketL.size() - 1;
		using cpair = std::pair<size_t,Cost>;
		std::vector< std::vector<cpair> > dp(k, std::vector<cpair>(n));
		// DP: we want to maximize the minimum max()-min() cluster
		// setup as:
		// dp[k][n]<len,cost-span>
		// where dp[i][j] signify cluster k ends at bucket j, with ith cluster size k holding span length cost-span
		// 0 <= i < k for cluster
		// 0 < j < n for bucket, note that bucket id starts as 1 and ends at n-1
		// init base DP
		for (int j = 1, je = n-k+1; j < je; ++j) {
			// from bucket j starts at 1, each bucket must be at least size of 1 thus leaving k-1 buckets unassigned
			auto& c = dp[0][j];
			c.first = j;
			c.second = bucketL[j]->max() - bucketL[1]->min();
		}
		// perform DP
		for (size_t ki = 1; ki < k; ++ki) {
			// calculate each cluster ki from 1 (0 was the base case)
			for (size_t i = ki+1, ie = n-(k-ki)+1; i < ie; ++i) {
				// starts at ki+1, as ki buckets are reserved for prior clusters
				cpair best(0, Cost::infinity());
				// dp[ki][i] = min( max( B[i].max()-B[j+1].min(), dp[ki][j] ) where ki <= j < i, 
				for (size_t j = ki; j < i; ++j) {
					Cost diff = bucketL[i]->max() - bucketL[j+1]->min();
					if (diff < dp[ki-1][j].second) {
						diff = dp[ki-1][j].second;
					}
					if (diff < best.second) {
						best.first = i - j;
						best.second = diff;
					}
				}
				dp[ki][i] = best;
			}
		}
		std::vector<size_t> clusterSize;
		clusterSize.resize(k);
		for (size_t ki = 0, kidx = n; ki < k; ++ki) {
			assert(kidx > 0);
			size_t tmp = dp[k-ki-1][kidx-1].first;
			assert(tmp <= kidx);
			kidx -= tmp;
			clusterSize[k-ki-1] = tmp;
		}
		clusters.resize(clusterNum);
		clusters.front().buckets.push_back(0);
		for (size_t i = 1, ki = 0; ki < k; ++ki) {
			Cluster& C = clusters[ki+1];
			C.buckets.reserve(clusterSize[ki]);
			for (size_t ie = i + clusterSize[ki]; i < ie; ++i) {
				C.buckets.push_back(i);
			}
		}
		clusters.back().buckets.push_back(bucketL.size()-1);
	}

	void outputBuckets(std::ostream& out) const
	{
		out << bucketL.size() << '\n';
		for (const Bucket* B : bucketL) {
			out << B->size();
			for (const CostId* c : B->items) {
				out << ' ' << c->id;
			}
			out << '\n';
		}
		out << std::flush;
	}

	void outputClusters(std::ostream& out) const
	{
		out << clusters.size() << '\n';
		for (const Cluster& C : clusters) {
			out << C.buckets.size() << '\n';
		}
		out << std::flush;
	}

	void outputScenarios(std::ostream& out) const
	{
		out << (bucketSize * clusterNum) << '\n';
		for (const Cluster& C : clusters) {
			Bucket& B = *bucketL[C.buckets.back()];
			size_t elem = bucketSize;
			for (auto it = B.items.end() - bucketSize, ite = B.items.end(); it != ite; ++it) {
				out << it[0]->id << (--elem != 0 ? ' ' : '\n');
			}
		}
		out << std::flush;
	}
	
private:
	std::vector<Cost> costList;
	std::vector<CostId> orderList;
	std::list<Bucket> bucketList;
	std::vector<Bucket*> bucketL;
	std::vector<Cluster> clusters;
	size_t bucketSize;
	size_t clusterNum;

	std::vector<CostId*> tmpBucket;

};

int main(int argc, char* argv[])
{
	// program examples:
	// benchmarkSelector input/- bucket-size cluster-count
	// benchmarkSelector input/- bucket-size cluster-count 
	// benchmarkSelector input/- bucket-size cluster-count

	if (argc == 2) {
		std::string help = argv[1];
		if (help == "--help") { // print help
			std::cout << "Example running" << std::endl;
			std::cout << "#1: benchmarkSelector costs-file|- bucket-size cluster-count" << std::endl;
			std::cout << "#2: benchmarkSelector costs-file|- bucket-size cluster-count bucket-file|-" << std::endl;
			std::cout << "#3: benchmarkSelector costs-file|- bucket-size cluster-count bucket-file|- cluster-file|-" << std::endl;
			std::cout << "#4: benchmarkSelector costs-file|- bucket-size cluster-count bucket-file|- cluster-file|- scenario-file|-" << std::endl;
			std::cout << "costs-file can be given a filename or - to read costs from stdin." << std::endl;
			std::cout << "bucket-size is the minimum number of costs per bucket, up to 2*bucket-size - 1." << std::endl;
			std::cout << "cluster-count is the number of clusters to generate, the number of costs should be at least 2 * bucket-size * cluster-count." << std::endl;
			std::cout << "bucket-file is the filename to output bucket info to, or - to output to stdout." << std::endl;
			std::cout << "cluster-file is the filename to output cluster info to, or - to output to stdout." << std::endl;
			std::cout << "scenario-file is the filename to output scenario info to, or - to output to stdout." << std::endl;
			std::cout << "if multiple stdout sources are specifed, they will all be printed, set filename to /dev/null to skip parameters." << std::endl;
			std::cout << "#1 will only output scenario-file to stdout." << std::endl;
			return 0;
		}
	}
	if (argc < 4 || argc > 7) {
		std::cerr << "invalid number of arguments." << std::endl;
		return 1;
	}
	std::string tmpStr;
	std::string inputF;
	size_t bucketSize;
	size_t clusterCount;
	std::string bucketF;
	std::string clusterF;
	std::string scenarioF;

	Selector selector;

	try {
		// read inputF
		inputF = argv[1];
		if (inputF == "-") {
			selector.loadCosts(std::cin);
		} else {
			std::ifstream file(inputF);
			if (!file) {
				std::cerr << "unable to open file \"" << inputF << "\"" << std::endl;
				return 2;
			}
			selector.loadCosts(file);
			if (file.fail()) {
				std::cerr << "failed to complete read of file \"" << inputF << "\"" << std::endl;
				return 2;
			}
		}
		size_t selectorSize = selector.size();

		int tmpInt;
		if (std::sscanf(argv[2], "%d", &tmpInt) != 1 || tmpInt < 1) {
			std::cerr << "invalid read of bucket-size \"" << inputF << "\"" << std::endl;
			return 2;
		}
		bucketSize = tmpInt;
		if (std::sscanf(argv[3], "%d", &tmpInt) != 1 || tmpInt < 2) {
			std::cerr << "invalid read of cluster-count \"" << inputF << "\"" << std::endl;
			return 2;
		}
		clusterCount = tmpInt;
		if (bucketSize > selectorSize || clusterCount > selectorSize || 2 * bucketSize * clusterCount > selectorSize) {
			std::cerr << "bucket-size * cluster-count cannot exceed half of the number of costs input" << std::endl;
			return 2;
		}

		// check output files
		int outputLevel = 0;
		if (argc == 4) {
			outputLevel = 3;
			bucketF = "/dev/null";
			clusterF = "/dev/null";
			scenarioF = "-";
		} else {
			if (argc > 4) {
				outputLevel += 1;
				bucketF = argv[4];
			}
			if (argc > 5) {
				outputLevel += 1;
				clusterF = argv[5];
			}
			if (argc > 6) {
				outputLevel += 1;
				scenarioF = argv[6];
			}
		}

		// copmute and output
		// generate buckets
		if (outputLevel >= 1) {
			selector.generateBuckets(bucketSize);
			if (bucketF == "-") {
				selector.outputBuckets(std::cout);
			} else {
				std::ofstream file(bucketF);
				if (!file) {
					std::cerr << "unable to open file \"" << inputF << "\"" << std::endl;
					return 2;
				}
				selector.outputBuckets(file);
				if (file.fail()) {
					std::cerr << "failed to complete read of file \"" << inputF << "\"" << std::endl;
					return 2;
				}
			}
		}
		// generate clusters
		if (outputLevel >= 2) {
			selector.generateClusters(clusterCount);
			if (clusterF == "-") {
				selector.outputClusters(std::cout);
			} else {
				std::ofstream file(clusterF);
				if (!file) {
					std::cerr << "unable to open file \"" << inputF << "\"" << std::endl;
					return 2;
				}
				selector.outputClusters(file);
				if (file.fail()) {
					std::cerr << "failed to complete read of file \"" << inputF << "\"" << std::endl;
					return 2;
				}
			}
		}
		// generate scenarios
		if (outputLevel >= 3) {
			if (scenarioF == "-") {
				selector.outputScenarios(std::cout);
			} else {
				std::ofstream file(scenarioF);
				if (!file) {
					std::cerr << "unable to open file \"" << inputF << "\"" << std::endl;
					return 2;
				}
				selector.outputScenarios(file);
				if (file.fail()) {
					std::cerr << "failed to complete read of file \"" << inputF << "\"" << std::endl;
					return 2;
				}
			}
		}
	}
	catch (std::exception& e) {
		std::cerr << "Error: " << e.what() << std::endl;
		return 3;
	}

}
